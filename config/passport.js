const JwtStrategy = require('passport-jwt').Strategy;
const LocalStrategy = require('passport-local').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;

const keys = require('../config/keys.json');
const User = require('../models/index').User;

const opts = {};
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = keys.secret;

module.exports = passport => {
  passport.use(new JwtStrategy(opts, (jwt_payload, done) => {
    User.findById(jwt_payload.id)
      .then(user => {
        if (user) {
          return done(null, user);
        }
        return done(null, false);
      })
      .catch(error => console.log(error))
  })),
    passport.use(new LocalStrategy({
      usernameField: 'email',
      passwordField: 'password',
      session: false
    }, (username, password, done) => {
      User.findOne({ where: { email: username.toLowerCase() } })
        .then(user => {
          // When a user is not found
          if (!user) return done(null, false);
          // When the password is not correct
          if (!(user.verifyPassword(password))) return done(null, false);
          // When all things are good, we return the user
          return done(null, user);
        }).catch(err => done(null, false))
    }));
}