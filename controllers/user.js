const gravatar = require('gravatar');
const jwt = require('jsonwebtoken');

const keys = require('../config/keys.json');
const User = require('../models/index').User;
const UserConstants = require('../constants/user');

module.exports = {
  /**
   * @name              allUser
   * @description       Retrieves all users
  */
  allUser(req, res) {
    return User.all({ raw: true })
      .then(users => res.status(200).send(users))
      .catch(error => res.status(400).send(error));
  },

  /**
   * @name              createUser
   * @description       Creates a new user
   * @param {string}    email     
   * @param {string}    name         
   * @param {string}    password         
  */
  createUser(req, res) {
    User.findOne({ where: { email: req.body.email.toLowerCase() } })
      .then(user => {
        if (user) {
          return res.status(400).send({ message: 'There is already an account with that email address.' })
        } else {
          /**
           * gravatar.url(email, options, protocol);
           * email: The gravatar email
           * options: Query string options. Ex: size or s, default or d, rating or r, forcedefault or f.
           * protocol Define if will use no protocol, http or https gravatar URL. Default is 'undefined', which generates URLs without protocol. True to force https and false to force http.
           */
          const avatar = gravatar.url(req.body.email, { s: '250', r: 'x', d: 'retro' }, true);

          return User.create({
            avatar,
            email: req.body.email,
            name: req.body.name,
            password: req.body.password
          })
            .then(() => res.status(201).send({ message: 'You have been registered!' }))
            .catch(error => res.status(400).send(error));
        }
      })
      .catch(error => res.status(400).send(error));
  },

  /**
   * @name              deleteUser
   * @description       Deletes an existing user by their id
   * @param {integer}   userId
  */
  deleteUser(req, res) {
    return User.findById(req.params.userId)
      .then(user => {
        if (!user) {
          return res.status(400).send({
            message: 'User Not Found',
          });
        }
        return user.destroy()
          .then(() => res.status(204).send({ message: 'User deleted successfully.' }))
          .catch(error => res.status(400).send(error));
      })
      .catch(error => res.status(400).send(error));
  },

  /**
   * @name              findUser
   * @description       Finds a user by their id
   * @param {integer}   userId
  */
  findUser(req, res) {
    return User.findById(req.params.userId, { raw: true })
      .then(user => {
        if (!user) {
          return res.status(404).send({
            message: 'User Not Found',
          });
        }
        return res.status(200).send({
          avatar: user.avatar,
          email: user.email,
          id: user.id,
          name: user.name,
          role: user.role
        });
      })
      .catch(error => res.status(400).send(error));
  },

  /**
   * @name              passwordForgot
   * @description       Sends a password reset email with a token
   * @param {integer}   userId
  */
  passwordForgot(req, res) {
    return User.findById(req.params.userId)
      .then(user => {
        if (!user) {
          return res.status(404).send({
            message: 'User Not Found',
          });
        }
        /** TODO: Need to send a password reset email, generate a token to be
         * used in a URL in that email, and then allow that token to be used in
         * passwordTokenUpdate to change the password. */
      })
      .catch(error => res.status(400).send(error));
  },

  /**
   * @name              passwordTokenUpdate
   * @description       Verifies a provided token and updates the password of
   *                    the user, by id, to the provided one
   * @param {string}    password
   * @param {string}    token
   * @param {integer}   userId
  */
  passwordTokenUpdate(req, res) {
    return User.findById(req.params.userId)
      .then(user => {
        if (!user) {
          return res.status(404).send({
            message: 'User Not Found',
          });
        }
        /** TODO: Validate the token and, if valid, update the user's password
         * to the one being sent. */
        return user
          .update({
            password: req.body.password
          })
          .then(() => res.status(200).send(user.get({ plain: true })))
          .catch(error => res.status(400).send(error));
      })
      .catch(error => res.status(400).send(error));
  },

  /**
   * @name              passwordUpdate
   * @description       Updates a signed in user's password to the provided one
   * @param {string}    password
   * @param {integer}   userId
  */
  passwordUpdate(req, res) {
    return User.findById(req.params.userId)
      .then(user => {
        if (!user) {
          return res.status(404).send({
            message: 'User Not Found',
          });
        }
        return user
          .update({
            password: req.body.password
          })
          .then(() => res.status(200).send(user.get({ plain: true })))
          .catch(error => res.status(400).send(error));
      })
      .catch(error => res.status(400).send(error));
  },

  /**
   * @name              roleUpdate
   * @description       Updates a user's role
   * @param {string}    role
   * @param {integer}   userId
  */
  roleUpdate(req, res) {
    return User.findById(req.params.userId)
      .then(user => {
        if (!user) {
          return res.status(404).send({
            message: 'User Not Found',
          });
        }
        return user
          .update({
            role: req.body.role
          })
          .then(() => res.status(200).send(user.get({ plain: true })))
          .catch(error => res.status(400).send(error));
      })
      .catch(error => res.status(400).send(error));
  },

  /**
   * @name              signUser
   * @description       Signs in a single user by their email and password
   * @param {string}    email
   * @param {string}    password
  */
  signUser(req, res) {
    const token = jwt.sign({ sub: req.user.id }, keys.secret, { expiresIn: UserConstants.TOKEN_EXPIRY });
    if (!token) {
      return res.status(400).send(error)
    }
    return res.send({
      token: 'Bearer ' + token,
      tokenExpiry: UserConstants.TOKEN_EXPIRY,
      user: {
        avatar: req.user.avatar,
        email: req.user.email,
        id: req.user.id,
        name: req.user.name,
        role: req.user.role
      }
    });
  },

  /**
   * @name              updateUser
   * @description       Updates a single user by their id
   * @param {string}    email
   * @param {string}    name
   * @param {integer}   userId
  */
  updateUser(req, res) {
    return User.findById(req.params.userId)
      .then(user => {
        if (!user) {
          return res.status(404).send({
            message: 'User Not Found',
          });
        }
        return user
          .update({
            email: req.body.email || user.email,
            name: req.body.name || user.name
          })
          .then(() => res.status(200).send(user.get({ plain: true })))
          .catch(error => res.status(400).send(error));
      })
      .catch(error => res.status(400).send(error));
  }
}