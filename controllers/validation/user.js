const constant = require('../constants/user');

module.exports.createUser = {
  body: {
    email: joi.string().trim().email().required(),
    name: joi.string().trim().required(),
    password: joi.string().required().min(6)
  }
}

module.exports.signUser = {
  body: {
    email: joi.string().trim().email().required(),
    password: joi.string().required().min(6)
  }
}

module.exports.deleteUser = {
  body: {
    id: joi.trim().number().required()
  }
}

module.exports.passwordForgot = {
  body: {
    id: joi.trim().number().required()
  }
}

module.exports.passwordTokenUpdate = {
  body: {
    id: joi.trim().number().required(),
    password: joi.string().required().min(6),
    token: joi.string().required().min(6)
  }
}

module.exports.passwordUpdate = {
  body: {
    id: joi.trim().number().required(),
    password: joi.string().required().min(6)
  }
}

module.exports.findUser = {
  body: {
    id: joi.trim().number().required()
  }
}

module.exports.roleUpdate = {
  body: {
    id: joi.trim().number().required(),
    role: joi.string().valid(constant.ROLE.ADMINISTRATOR, constant.ROLE.MODERATOR, constant.ROLE.USER).required()
  }
}

module.exports.updateUser = {
  body: {
    email: joi.string().trim().email(),
    id: joi.trim().number().required(),
    name: joi.string().trim()
  }
}