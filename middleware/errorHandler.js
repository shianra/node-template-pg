const boom = require('boom');

/**
 * Middleware used to handle errors.
 * 
 * boom package: boom provides a set of utilities for returning HTTP errors.
 * 
 * If the errors are validation errors from the controllers (status code = 422), we compile them into an array and we use boom to create a custom error that will be sent to the client.
 * If the error is not a validation error, it gets sent to the client after making sure it gets converted to a boom error.
 */
module.exports.errorHandler = (err, req, res, next) => {
  // check if the error is not an instance of boom and it is a controller validation error
  if (!boom.isBoom(err) && err.status === 422) {
    let errors = [];
    // building up an array with all the validation errors descriptions
    for (let error of err.errors) {
      for (let errorMsg of error.messages) {
        errors.push(errorMsg);
      }
    }
    // ovewriting the error with a boom instance with the same status code (422)
    err = new boom(errors, { statusCode: err.status });
  } else if (!boom.isBoom(err)) {
    err = new boom(err.message);
  }
  return res.status(err.output.statusCode).json(err.output.payload);
}
