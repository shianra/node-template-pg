const request = require('supertest');

const app = require('../app');

describe('Server Tests', () => {

  describe('GET /api', () => {
    it('has the default page', function (done) {
      request(app)
        .get('/api')
        .expect(/Welcome to Express/, done)
    })
  })

})
