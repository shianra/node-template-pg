const constant = require('../constants/user');

module.exports = [
  {
    model: 'User',
    keys: [
      'email'
    ],
    data: {
      avatar: '',
      email: 'admin@test.com',
      name: 'Test Admin',
      password: 'password',
      role: constant.ROLE.ADMINISTRATOR
    }
  }
]