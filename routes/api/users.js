const express = require('express');
const passport = require('passport');
const router = express.Router();
require('../../config/passport')(passport);

const userController = require('../../controllers/index').User;

const auth = passport.authenticate('jwt', { session: false });
const login = passport.authenticate('local', { session: false });

/**
 * @route         GET api/users/test
 * @description   Tests users route. 
 * @access        Public
 */
router.get('/test', (req, res) => res.json({
  message: 'Users route works.'
}));

/**
 * @route         GET api/users/list
 * @description   List all users
 * @access        Public
 */
router.get('/list', userController.allUser);

/**
 * @route         POST api/users/login
 * @description   Login user
 * @access        Public
 */
router.post('/login', login, userController.signUser);

/**
 * @route         GET api/users/passwordForgot/:userId
 * @description   Sends a link to a user's email address for them to reset their password
 * @access        Public
 */
router.get('/passwordForgot/:userId', userController.passwordForgot);

/**
 * @route         PUT api/users/passwordTokenUpdate/:userId
 * @description   Update one user's password by their id but a token is required
 * @access        Public
 */
router.put('/passwordTokenUpdate/:userId', userController.passwordTokenUpdate);

/**
 * @route         PUT api/users/passwordUpdate/:userId
 * @description   Update one user's password by their id
 * @access        Private
 */
router.put('/passwordUpdate/:userId', auth, userController.passwordUpdate);

/**
 * @route         PUT api/users/roleUpdate/:userId
 * @description   Update one user's role by their id
 * @access        Private
 */
router.put('/roleUpdate/:userId', auth, userController.roleUpdate);

/**
 * @route         POST api/users/register
 * @description   Register user
 * @access        Public
 */
router.post('/register', userController.createUser);

/**
 * @route         DELETE api/users/:userId
 * @description   Delete user
 * @access        Private
 */
router.delete('/:userId', auth, userController.deleteUser);

/**
 * @route         GET api/users/:userId
 * @description   Retrieve one user by their id
 * @access        Public
 */
router.get('/:userId', userController.findUser);

/**
 * @route         PUT api/users/:userId
 * @description   Update one user by their id
 * @access        Private
 */
router.put('/:userId', auth, userController.updateUser);

module.exports = router;
